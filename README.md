Se você está recebendo este teste, é porque você passou pela primeira etapa da nossa seleção, Parabéns! O objetivo deste desafio técnico, é conhecer melhor suas habilidades, técnicas de programação e boas praticas.

## **O que deverá ser desenvolvido:**

Deverá ser desenvolvido um sistema de votação online, onde um usuário poderá acessar o site, preencher seu nome e e-mail, e votar em uma das opções exibidas na tela. 
O sistema deverá ter uma área administrativa, onde, após realizar o login, o administrador do sistema poderá ver a quantidade de votos para cada opção, cadastrar novas opções ou excluir opções existentes.

**Instruções:**

- Deverá ser utilizada a ultima versão do framework Laravel;
- Um mesmo e-mail não poderá votar mais de uma vez;
- Não precisa fazer sistema para cadastro de usuários administradores, o login e senha do administrador deve ser inserido direto no banco por meio de uma classe seed do Laravel ([https://laravel.com/docs/8.x/seeding](https://laravel.com/docs/8.x/seeding));
- O banco de dados deve ser montado utilizando migrations ([https://laravel.com/docs/8.x/migrations](https://laravel.com/docs/8.x/migrations));
- Os componentes de cada tela estão exemplificados pelo wireframe que está neste repositório;
- Deverá ser utilizada a biblioteca bootstrap para os componentes de front-end;
- Não é necessário seguir o mesmo design do wireframe, ele serve apenas para mostrar o que deve conter em cada página;
- O projeto deverá conter um arquivo [README.md](http://readme.md) com os requisitos e instruções para executar o projeto.